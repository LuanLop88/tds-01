﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace PRJ_Agenda
{
    public partial class Tela_de_Contato : Form
    {
        public Tela_de_Contato()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
          
        }

        private void Lista()
        {
            MySqlConnection conn;
            string conString;

            conString = "server = 10.1.1.3; user = tds01_luan; password = 123; database = tds01_luan;";
            conn = new MySqlConnection(conString);
            conn.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select * from agenda", conn);
            DataSet ds = new DataSet();

            adapter.Fill(ds);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = ds.Tables[0];

            conn.Close();
        }

        private void btnIncluir_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MySqlConnection Conn;
                MySqlCommand _Comando;
                string conString;

                conString = "server = 10.1.1.3; user = tds01_luan; password = 123; database = tds01_luan;";
                Conn = new MySqlConnection(conString);

                Conn.Open();

                _Comando = new MySqlCommand("Insert Into agenda (id, nome, endereco, cidade, estado,telefone,email, whatsapp,facebook,linkedin,twitter,instagran,observacao) values(@id,@nome,@endereco,@cidade,@estado,@telefone,@email,@whatsapp,@facebook,@linkedin,@twitter,@instagran,@observacao)", Conn);
                _Comando.Parameters.AddWithValue("id", txtid.Text);
                _Comando.Parameters.AddWithValue("nome", txtNome.Text);
                _Comando.Parameters.AddWithValue("endereco", txtEndereco.Text);
                _Comando.Parameters.AddWithValue("cidade", txtCidade.Text);
                _Comando.Parameters.AddWithValue("estado", txtEstado.Text);
                _Comando.Parameters.AddWithValue("telefone", txtTelefone.Text);
                _Comando.Parameters.AddWithValue("email", txtEmail.Text);
                _Comando.Parameters.AddWithValue("whatsapp", txtWhats.Text);
                _Comando.Parameters.AddWithValue("facebook", txtFacebook.Text);
                _Comando.Parameters.AddWithValue("linkedin", txtLinkedin.Text);
                _Comando.Parameters.AddWithValue("twitter", txtTwitter.Text);
                _Comando.Parameters.AddWithValue("instagran", txtInstagran.Text);
                _Comando.Parameters.AddWithValue("observacao", txtObs.Text);
                _Comando.ExecuteNonQuery();

                Lista();

                Conn.Close();

                MessageBox.Show("Contato Salvo");

                txtid.Text = "";
                txtNome.Text = "";
                txtEmail.Text = "";
                txtEndereco.Text = "";
                txtCidade.Text = "";
                txtFacebook.Text = "";
                txtTelefone.Text = "";
                txtTwitter.Text = "";
                txtInstagran.Text = "";
                txtObs.Text = "";
                txtLinkedin.Text = "";
                txtWhats.Text = "";
                txtEstado.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void txtid_Leave(object sender, EventArgs e)
        {
            
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlConnection Conn;
               // MySqlCommand _Comando;
                string conString;

                conString = "server = 10.1.1.3; user = tds01_luan; password = 123; database = tds01_luan;";
                Conn = new MySqlConnection(conString);

                Conn.Open();

                MySqlCommand ConnDell = new MySqlCommand("delete from agenda where id = ?", Conn);
                ConnDell.Parameters.Clear();
                ConnDell.Parameters.Add("@id", MySqlDbType.Int32).Value = txtid.Text;

                ConnDell.CommandType = CommandType.Text;
                ConnDell.ExecuteNonQuery();
                Conn.Close();
                MessageBox.Show("Registro Removido");
                txtid.Text = "";
                txtNome.Text = "";
                txtEmail.Text = "";
                txtEndereco.Text = "";
                txtCidade.Text = "";
                txtFacebook.Text = "";
                txtTelefone.Text = "";
                txtTwitter.Text = "";
                txtInstagran.Text = "";
                txtObs.Text = "";
                txtLinkedin.Text = "";
                txtWhats.Text = "";
                txtEstado.Text = "";
                Lista();

            }

            catch(Exception erro)
            {
                MessageBox.Show("Nao Foi Possivel Excluir" + erro);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            /* try
             {
                 MySqlConnection Conn;
                 string conString;

                 conString = "server = 10.1.1.3; user = tds01_luan; password = 123; database = tds01_luan;";
                 Conn = new MySqlConnection(conString);

                 Conn.Open();

                 MySqlCommand Conn_ConS = new MySqlCommand("Select nome, endereco, cidade, estado,telefone,email, whatsapp,facebook,linkedin,twitter,instagran,observacao from agenda where id = ?", Conn);
                 Conn_ConS.Parameters.Clear();
                 Conn_ConS.Parameters.Add("@id", MySqlDbType.Int32).Value = txtid.Text;

                 Conn_ConS.CommandType = CommandType.Text;

                 MySqlDataReader ConS;
                 ConS = Conn_ConS.ExecuteReader();

                 ConS.Read();

                 txtNome.Text = ConS.GetString(0);
                 txtEndereco.Text = ConS.GetString(1);
                 txtCidade.Text = ConS.GetString(2);
                 txtEstado.Text = ConS.GetString(3);
                 txtTelefone.Text = ConS.GetString(4);
                 txtEmail.Text = ConS.GetString(5);
                 txtWhats.Text = ConS.GetString(6);
                 txtFacebook.Text = ConS.GetString(7);
                 txtLinkedin.Text = ConS.GetString(8);
                 txtTwitter.Text = ConS.GetString(9);
                 txtInstagran.Text = ConS.GetString(10);
                 txtObs.Text = ConS.GetString(11);

                 Conn.Close();
             }

             catch(Exception erro)
             {
                 MessageBox.Show("Registro Não encontrado" + erro);
             }*/

            try
            {
                if (txtid.Text.Length <= 0)
                {
                    MessageBox.Show("Nro do ID Inválido");
                    return;
                }
                MySqlConnection conn;
                string conString;

                conString = "server = 10.1.1.3; user = tds01_luan; password = 123; database = tds01_luan;";
                conn = new MySqlConnection(conString);
                conn.Open();

                MySqlDataAdapter adapter = new MySqlDataAdapter("select * from agenda where id = " + txtid.Text, conn);
                DataSet ds = new DataSet();

                adapter.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtNome.Text = ds.Tables[0].Rows[0]["Nome"].ToString();
                    txtEndereco.Text = ds.Tables[0].Rows[0]["Endereco"].ToString();
                    txtCidade.Text = ds.Tables[0].Rows[0]["Cidade"].ToString();
                    txtEstado.Text = ds.Tables[0].Rows[0]["Estado"].ToString();
                    txtTelefone.Text = ds.Tables[0].Rows[0]["telefone"].ToString();
                    txtEmail.Text = ds.Tables[0].Rows[0]["email"].ToString();
                    txtWhats.Text = ds.Tables[0].Rows[0]["whatsapp"].ToString();
                    txtFacebook.Text = ds.Tables[0].Rows[0]["facebook"].ToString();
                    txtLinkedin.Text = ds.Tables[0].Rows[0]["linkedin"].ToString();
                    txtInstagran.Text = ds.Tables[0].Rows[0]["instagran"].ToString();
                    txtTwitter.Text = ds.Tables[0].Rows[0]["twitter"].ToString();
                    txtObs.Text = ds.Tables[0].Rows[0]["observacao"].ToString();
                    

                }
                else
                {
                    txtNome.Text = "";
                    txtEndereco.Text = "";
                    txtCidade.Text = "";
                    txtEstado.Text = "";
                    txtTelefone.Text = "";
                    txtEmail.Text = "";
                    txtWhats.Text = "";
                    txtFacebook.Text = "";
                    txtLinkedin.Text = "";
                    txtInstagran.Text = "";
                    txtObs.Text = "";

                    MessageBox.Show("Não foi encontrado nenhum registro");
                    
                }
                
                conn.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return;
            }

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlConnection Conn;
                MySqlCommand _Comando;
                string conString;

                conString = "server = 10.1.1.3; user = tds01_luan; password = 123; database = tds01_luan;";
                Conn = new MySqlConnection(conString);

                Conn.Open();


                _Comando = new MySqlCommand("update agenda set nome=@nome, endereco=@endereco, cidade=@cidade, estado=@estado, telefone=@telefone," +
                " email = @email, whatsapp = @whatsapp, facebook = @facebook, linkedin = @linkedin, twitter = @twitter, instagran = @instagran, observacao = @observacao  where id = @id ", Conn);

                _Comando.Parameters.AddWithValue("@id", txtid.Text);
                _Comando.Parameters.AddWithValue("@nome",  txtNome.Text);
                _Comando.Parameters.AddWithValue("@endereco",  txtEndereco.Text);
                _Comando.Parameters.AddWithValue("@cidade", txtCidade.Text);
                _Comando.Parameters.AddWithValue("@estado", txtEstado.Text);
                _Comando.Parameters.AddWithValue("@telefone", txtTelefone.Text);
                _Comando.Parameters.AddWithValue("@email",  txtEmail.Text);
                _Comando.Parameters.AddWithValue("@whatsapp",  txtWhats.Text);
                _Comando.Parameters.AddWithValue("@facebook", txtFacebook.Text);
                _Comando.Parameters.AddWithValue("@linkedin", txtLinkedin.Text);
                _Comando.Parameters.AddWithValue("@twitter",  txtTwitter.Text);
                _Comando.Parameters.AddWithValue("@instagran",  txtInstagran.Text);
                _Comando.Parameters.AddWithValue("@observacao", txtObs.Text);
                _Comando.ExecuteNonQuery();

                //_Comando.CommandType = CommandType.Text;

                Lista();
                Conn.Close();

                MessageBox.Show("Registro Alterado");

                txtid.Text = "";
                txtNome.Text = "";
                txtEmail.Text = "";
                txtEndereco.Text = "";
                txtCidade.Text = "";
                txtFacebook.Text = "";
                txtTelefone.Text = "";
                txtTwitter.Text = "";
                txtInstagran.Text = "";
                txtObs.Text = "";
                txtLinkedin.Text = "";
                txtWhats.Text = "";
                txtEstado.Text = "";

            }

            catch (Exception erro)
            {
                MessageBox.Show("Registro nao possivel alterar" + erro);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
