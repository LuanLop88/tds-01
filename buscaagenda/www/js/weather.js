//var OpenWeatherAppKey = "132bba4f20384f1c1e1443faa0e5cb73";

function getWeatherWithZipCode() {
    var zipcode = $('#zip-code-input').val();
    var queryString =
        ' localhost:80/agenda.php='  + zipcode + 'id=' ;
    $.getJSON(queryString, function(results) {
        showWeatherData(results);
        console.log(results);
    }).fail(function(jqXHR) {
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. " + jqXHR.statusText);
        console.log(jqXHR.statusText);
    });
    return false;
}

function showWeatherData(results) {

    if (results.length) {
        $('#error-msg').hide();
        $('#weather-data').show();

        $('#id').text(results.id);
        $('#nome').text(results.nome);
        $('#endereco').text(results.endereco);
        $('#cidae').text(results.cidade);
        $('#estado').text(results.estado);
        $('#email').text(results.email);
        $('#telefone').text(results.telefone);
        $('#whatsapp').text(results.whatsapp);
        $('#facebook').text(results.facebook);
        $('#linkedin').text(results.linkedin);
        $('#twitter').text(results.twitter);
        $('#instagran').text(results.instagran);
        $('#observacao').text(results.observacao);

      //  var sunriseDate = new Date(results.sys.sunrise * 1000);
      //  $('#sunrise').text(sunriseDate.toLocaleTimeString());

     //   var sunsetDate = new Date(results.sys.sunset * 1000);
     //   $('#sunset').text(sunsetDate.toLocaleTimeString());

    } else {
      //  $('#weather-data').hide();
     //   $('#error-msg').show();
     //   $('#error-msg').text("Error retrieving data. ");
    }
}